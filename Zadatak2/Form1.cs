﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Text;

namespace Zadatak2
{
    public partial class Vjesala : Form
    {
        List<Char> slova = new List<char>();
        List<String> rijeci = new List<string>();
        string rijec;
        char slovo = 'A';
        int pokusaji = 0;
        public Vjesala()
        {
            InitializeComponent();

            for (int i = 'A'; i < 'Z'; i++)
            {
                slova.Add((char)i);
            }
            cbSlova.DataSource = slova;
            string dir = "D:\\file.txt";
            using (StreamReader sr = new StreamReader(dir))
            {
                string line;
                while((line = sr.ReadLine()) != null)
                {
                    rijeci.Add(line.ToUpper());
                }
            }
            Random rnd = new Random();
            rijec = rijeci[rnd.Next(rijeci.Count)].ToUpper();
            for (int i = 0; i < rijec.Length; i++) {
                lRjesenje.Text += "_ ";
            }

            pokusaji = 10;
            lBrojPokusaja.Text = pokusaji.ToString();
        }
        private void bOdabirSlova_Click(object sender, EventArgs e)
        {
            int id;
            slovo = (char)cbSlova.SelectedItem;
            if (pokusaji > 0)
            {
                if (rijec.Contains(slovo))
                {

                    while (rijec.Contains(slovo))
                    {
                        id = rijec.IndexOf(slovo);
                        int temp = (id == 0) ? 0 : id * 2;
                        StringBuilder tempS = new StringBuilder(lRjesenje.Text);
                        StringBuilder tempSS = new StringBuilder(rijec);
                        tempS[temp] = slovo;
                        lRjesenje.Text = tempS.ToString();
                        tempSS[id] = '*';
                        rijec = tempSS.ToString();
                    }

                }
                else
                {
                    pokusaji--;
                }
            }
            else {
                MessageBox.Show("Nije preostalo više pokušaja.", "Kraj igre");
            }
            slova.Remove(slovo);
            cbSlova.DataSource = null;
            cbSlova.DataSource = slova;
            lBrojPokusaja.Text = pokusaji.ToString();
            if (!lRjesenje.Text.Contains('_')) {
                MessageBox.Show("Pogodili ste riječ.", "Pobjeda");
            }
        }

    }
}
