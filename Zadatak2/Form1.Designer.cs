﻿namespace Zadatak2
{
    partial class Vjesala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lBrojPokusaja = new System.Windows.Forms.Label();
            this.cbSlova = new System.Windows.Forms.ComboBox();
            this.bOdabirSlova = new System.Windows.Forms.Button();
            this.lRjesenje = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pokušaja:";
            // 
            // lBrojPokusaja
            // 
            this.lBrojPokusaja.AutoSize = true;
            this.lBrojPokusaja.Location = new System.Drawing.Point(50, 10);
            this.lBrojPokusaja.Name = "lBrojPokusaja";
            this.lBrojPokusaja.Size = new System.Drawing.Size(13, 13);
            this.lBrojPokusaja.TabIndex = 1;
            this.lBrojPokusaja.Text = "0";
            // 
            // cbSlova
            // 
            this.cbSlova.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSlova.FormattingEnabled = true;
            this.cbSlova.Location = new System.Drawing.Point(3, 26);
            this.cbSlova.Name = "cbSlova";
            this.cbSlova.Size = new System.Drawing.Size(91, 21);
            this.cbSlova.TabIndex = 2;
            // 
            // bOdabirSlova
            // 
            this.bOdabirSlova.Location = new System.Drawing.Point(3, 54);
            this.bOdabirSlova.Name = "bOdabirSlova";
            this.bOdabirSlova.Size = new System.Drawing.Size(91, 23);
            this.bOdabirSlova.TabIndex = 3;
            this.bOdabirSlova.Text = "Odaberi slovo";
            this.bOdabirSlova.UseVisualStyleBackColor = true;
            this.bOdabirSlova.Click += new System.EventHandler(this.bOdabirSlova_Click);
            // 
            // lRjesenje
            // 
            this.lRjesenje.AutoSize = true;
            this.lRjesenje.Location = new System.Drawing.Point(3, 98);
            this.lRjesenje.Name = "lRjesenje";
            this.lRjesenje.Size = new System.Drawing.Size(0, 13);
            this.lRjesenje.TabIndex = 4;
            // 
            // Vjesala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 123);
            this.Controls.Add(this.lRjesenje);
            this.Controls.Add(this.bOdabirSlova);
            this.Controls.Add(this.cbSlova);
            this.Controls.Add(this.lBrojPokusaja);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Vjesala";
            this.Text = "Vjesala";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lBrojPokusaja;
        private System.Windows.Forms.ComboBox cbSlova;
        private System.Windows.Forms.Button bOdabirSlova;
        private System.Windows.Forms.Label lRjesenje;
    }
}

