﻿namespace Zadatak1
{
    partial class Calc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.bComma = new System.Windows.Forms.Button();
            this.tb = new System.Windows.Forms.TextBox();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonCE = new System.Windows.Forms.Button();
            this.bLog = new System.Windows.Forms.Button();
            this.bCos = new System.Windows.Forms.Button();
            this.bSin = new System.Windows.Forms.Button();
            this.bSq = new System.Windows.Forms.Button();
            this.bDiv = new System.Windows.Forms.Button();
            this.bTimes = new System.Windows.Forms.Button();
            this.bSqrt = new System.Windows.Forms.Button();
            this.bMinus = new System.Windows.Forms.Button();
            this.bPlus = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.buttonPM = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-1, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(33, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 30);
            this.button2.TabIndex = 1;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(69, 41);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(30, 30);
            this.button3.TabIndex = 2;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(-1, 76);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(30, 30);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(33, 76);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(30, 30);
            this.button5.TabIndex = 4;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(69, 76);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(30, 30);
            this.button6.TabIndex = 5;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(-1, 111);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(30, 30);
            this.button7.TabIndex = 6;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(33, 111);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(30, 30);
            this.button8.TabIndex = 7;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(69, 111);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(30, 30);
            this.button9.TabIndex = 8;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(33, 146);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(32, 30);
            this.button0.TabIndex = 9;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // bComma
            // 
            this.bComma.Location = new System.Drawing.Point(69, 146);
            this.bComma.Name = "bComma";
            this.bComma.Size = new System.Drawing.Size(30, 30);
            this.bComma.TabIndex = 10;
            this.bComma.Text = ".";
            this.bComma.UseVisualStyleBackColor = true;
            this.bComma.Click += new System.EventHandler(this.bComma_Click);
            // 
            // tb
            // 
            this.tb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb.Location = new System.Drawing.Point(0, 0);
            this.tb.Margin = new System.Windows.Forms.Padding(0);
            this.tb.Name = "tb";
            this.tb.ReadOnly = true;
            this.tb.Size = new System.Drawing.Size(285, 38);
            this.tb.TabIndex = 11;
            this.tb.Text = "0";
            this.tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonC
            // 
            this.buttonC.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonC.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC.Location = new System.Drawing.Point(218, 71);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(66, 30);
            this.buttonC.TabIndex = 13;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = false;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonCE
            // 
            this.buttonCE.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonCE.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonCE.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCE.Location = new System.Drawing.Point(218, 41);
            this.buttonCE.Margin = new System.Windows.Forms.Padding(0);
            this.buttonCE.Name = "buttonCE";
            this.buttonCE.Size = new System.Drawing.Size(66, 30);
            this.buttonCE.TabIndex = 14;
            this.buttonCE.Text = "CE";
            this.buttonCE.UseVisualStyleBackColor = false;
            this.buttonCE.Click += new System.EventHandler(this.buttonCE_Click);
            // 
            // bLog
            // 
            this.bLog.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bLog.FlatAppearance.BorderSize = 0;
            this.bLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.bLog.Location = new System.Drawing.Point(177, 110);
            this.bLog.Name = "bLog";
            this.bLog.Size = new System.Drawing.Size(40, 40);
            this.bLog.TabIndex = 23;
            this.bLog.Text = "log";
            this.bLog.UseVisualStyleBackColor = false;
            this.bLog.Click += new System.EventHandler(this.bLog_Click);
            // 
            // bCos
            // 
            this.bCos.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bCos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bCos.FlatAppearance.BorderSize = 0;
            this.bCos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.bCos.Location = new System.Drawing.Point(141, 110);
            this.bCos.Name = "bCos";
            this.bCos.Size = new System.Drawing.Size(40, 40);
            this.bCos.TabIndex = 22;
            this.bCos.Text = "cos";
            this.bCos.UseVisualStyleBackColor = false;
            this.bCos.Click += new System.EventHandler(this.bCos_Click);
            // 
            // bSin
            // 
            this.bSin.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bSin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bSin.FlatAppearance.BorderSize = 0;
            this.bSin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.bSin.Location = new System.Drawing.Point(101, 110);
            this.bSin.Name = "bSin";
            this.bSin.Size = new System.Drawing.Size(40, 40);
            this.bSin.TabIndex = 21;
            this.bSin.Text = "sin";
            this.bSin.UseVisualStyleBackColor = false;
            this.bSin.Click += new System.EventHandler(this.bSin_Click);
            // 
            // bSq
            // 
            this.bSq.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bSq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bSq.FlatAppearance.BorderSize = 0;
            this.bSq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSq.Location = new System.Drawing.Point(177, 75);
            this.bSq.Name = "bSq";
            this.bSq.Size = new System.Drawing.Size(40, 40);
            this.bSq.TabIndex = 20;
            this.bSq.Text = "^";
            this.bSq.UseVisualStyleBackColor = false;
            this.bSq.Click += new System.EventHandler(this.bSq_Click);
            // 
            // bDiv
            // 
            this.bDiv.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bDiv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bDiv.FlatAppearance.BorderSize = 0;
            this.bDiv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDiv.Location = new System.Drawing.Point(141, 75);
            this.bDiv.Name = "bDiv";
            this.bDiv.Size = new System.Drawing.Size(40, 40);
            this.bDiv.TabIndex = 19;
            this.bDiv.Text = "/";
            this.bDiv.UseVisualStyleBackColor = false;
            this.bDiv.Click += new System.EventHandler(this.bDiv_Click);
            // 
            // bTimes
            // 
            this.bTimes.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bTimes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bTimes.FlatAppearance.BorderSize = 0;
            this.bTimes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bTimes.Location = new System.Drawing.Point(101, 75);
            this.bTimes.Name = "bTimes";
            this.bTimes.Size = new System.Drawing.Size(40, 40);
            this.bTimes.TabIndex = 18;
            this.bTimes.Text = "*";
            this.bTimes.UseVisualStyleBackColor = false;
            this.bTimes.Click += new System.EventHandler(this.bTimes_Click);
            // 
            // bSqrt
            // 
            this.bSqrt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bSqrt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bSqrt.FlatAppearance.BorderSize = 0;
            this.bSqrt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSqrt.Location = new System.Drawing.Point(177, 41);
            this.bSqrt.Name = "bSqrt";
            this.bSqrt.Size = new System.Drawing.Size(40, 40);
            this.bSqrt.TabIndex = 17;
            this.bSqrt.Text = "root";
            this.bSqrt.UseVisualStyleBackColor = false;
            this.bSqrt.Click += new System.EventHandler(this.bSqrt_Click);
            // 
            // bMinus
            // 
            this.bMinus.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bMinus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bMinus.FlatAppearance.BorderSize = 0;
            this.bMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bMinus.Location = new System.Drawing.Point(141, 41);
            this.bMinus.Name = "bMinus";
            this.bMinus.Size = new System.Drawing.Size(40, 40);
            this.bMinus.TabIndex = 16;
            this.bMinus.Text = "-";
            this.bMinus.UseVisualStyleBackColor = false;
            this.bMinus.Click += new System.EventHandler(this.bMinus_Click);
            // 
            // bPlus
            // 
            this.bPlus.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bPlus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bPlus.FlatAppearance.BorderSize = 0;
            this.bPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPlus.Location = new System.Drawing.Point(101, 41);
            this.bPlus.Name = "bPlus";
            this.bPlus.Size = new System.Drawing.Size(40, 40);
            this.bPlus.TabIndex = 15;
            this.bPlus.Text = "+";
            this.bPlus.UseVisualStyleBackColor = false;
            this.bPlus.Click += new System.EventHandler(this.bPlus_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(101, 146);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(116, 30);
            this.button10.TabIndex = 24;
            this.button10.Text = "=";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // buttonPM
            // 
            this.buttonPM.Location = new System.Drawing.Point(0, 146);
            this.buttonPM.Name = "buttonPM";
            this.buttonPM.Size = new System.Drawing.Size(30, 30);
            this.buttonPM.TabIndex = 25;
            this.buttonPM.Text = "+/-";
            this.buttonPM.UseVisualStyleBackColor = true;
            this.buttonPM.Click += new System.EventHandler(this.buttonPM_Click);
            // 
            // Calc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 181);
            this.Controls.Add(this.buttonPM);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.bLog);
            this.Controls.Add(this.bCos);
            this.Controls.Add(this.bSin);
            this.Controls.Add(this.bSq);
            this.Controls.Add(this.bDiv);
            this.Controls.Add(this.bTimes);
            this.Controls.Add(this.bSqrt);
            this.Controls.Add(this.bMinus);
            this.Controls.Add(this.bPlus);
            this.Controls.Add(this.buttonCE);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.tb);
            this.Controls.Add(this.bComma);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Calc";
            this.Text = "Calc";
            this.Load += new System.EventHandler(this.Calc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button bComma;
        private System.Windows.Forms.TextBox tb;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonCE;
        private System.Windows.Forms.Button bLog;
        private System.Windows.Forms.Button bCos;
        private System.Windows.Forms.Button bSin;
        private System.Windows.Forms.Button bSq;
        private System.Windows.Forms.Button bDiv;
        private System.Windows.Forms.Button bTimes;
        private System.Windows.Forms.Button bSqrt;
        private System.Windows.Forms.Button bMinus;
        private System.Windows.Forms.Button bPlus;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button buttonPM;
    }
}

