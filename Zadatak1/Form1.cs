﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Zadatak1
{
    public partial class Calc : Form
    {
        double a = 0;
        double b = 0;
        enum Operation {Add, Sub, Mult, Div, Root, Pow, Log, Sin, Cos};
        int operation = (int)Operation.Add;

        public Calc()
        {
            InitializeComponent();
        }

        private void Calc_Load(object sender, EventArgs e)
        {

        }

        private void inputNum(int num) {
            string n = num.ToString();
            if (tb.Text.Contains('.'))
            {
                int i = tb.Text.IndexOf('.');
                if (tb.Text[i + 1] == '0' && tb.Text.Length == i + 2)
                {
                    tb.Text = tb.Text.Substring(0, tb.Text.Length - 1) + n;
                }
                else
                {
                    tb.Text += n;
                }
            }
            else
            {
                if (tb.Text == "0") tb.Text = "";
                tb.Text += n;
            }
        }

        private void setOperation(int op) {
            operation = op;
            a = double.Parse(tb.Text);
            tb.Text = "0";
        }

        private void bDiv_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Div);
        }

        private void bLog_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Log);
        }

        private void bCos_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Cos);
        }

        private void bSin_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Sin);
        }

        private void bSq_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Pow);
        }

        private void bTimes_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Mult);
        }

        private void bSqrt_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Root);
        }

        private void bPlus_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Add);
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            setOperation((int)Operation.Sub);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            inputNum(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            inputNum(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            inputNum(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            inputNum(4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            inputNum(5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            inputNum(6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            inputNum(7);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            inputNum(8);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            inputNum(9);
        }

        private void button0_Click(object sender, EventArgs e)
        {
            if (!tb.Text.Contains('.') && (int)double.Parse(tb.Text) != 0) tb.Text += "0";
        }

        private void bComma_Click(object sender, EventArgs e)
        {
            if (!tb.Text.Contains(".")) tb.Text += ".0";
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            tb.Text = "0";
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            if (tb.Text.Length > 1)
            {
                tb.Text = tb.Text.Substring(0, tb.Text.Length - 1);
                if (tb.Text[tb.Text.Length - 1] == '.') tb.Text = tb.Text.Substring(0, tb.Text.Length - 1);
            }
            else {
                tb.Text = "0";
            }
        }

        private void buttonPM_Click(object sender, EventArgs e)
        {
            double temp = double.Parse(tb.Text);
            tb.Text = (-temp).ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            b = double.Parse(tb.Text);
            double result = 0;
            switch (operation) {
                case (int)Operation.Add:
                    result = a + b;
                    break;
                case (int)Operation.Sub:
                    result = a - b;
                    break;
                case (int)Operation.Mult:
                    result = a * b;
                    break;
                case (int)Operation.Div:
                    result = (b == 0) ? 0 : a / b;
                    break;
                case (int)Operation.Log:
                    result = (a == 1)? b: Math.Log(b, a); //logaritam po bazi a od b
                    break;
                case (int)Operation.Pow:
                    result = Math.Pow(a, b); //a "na" b
                    break;
                //sljedece operacije se izvrsavaju samo na b, dakle na broj nakon unesene operacije
                case (int)Operation.Sin:
                    result = Math.Sin(b);
                    break;
                case (int)Operation.Cos:
                    result = Math.Cos(b);
                    break;
                //ati korjen iz b
                case (int)Operation.Root:
                    result = (a == 0) ? 0 : Math.Pow(b, 1/a);
                    break;
            }
            tb.Text = result.ToString();
        }
    }
}
